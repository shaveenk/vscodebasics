#include <iostream>
#include <vector>
using namespace std;


void printValues(vector<int>& array)
{
    for (auto& val : array)
    {
        cout << val << endl;
    }    
}

int main()
{
    vector<int> array = {1,2,3,4,5};
    printValues(array);

    return 0;
}