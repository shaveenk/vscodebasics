# VSCodeBasics

## Description

This is a simple project created using VS code on Linux. It demonstrates how to configure the C++ compiler and debugger and also some tips to take its functionality closer to Visual Studio. The project also uses a Makefile for generating builds on Linux and Tasks to trigger various events like build, clean, etc.

## Tips

**Terminal Commands**  

***Generate Assembly File***  
g++ -S Test.cpp -o Test.s

***Generate executable with debug symbols***  
g++ -g Test.cpp -o Test

***Compile for the C++11 standard***  
g++ -std=c++11 Test.cpp -o Test

***Disassemble the binary file***  
objdump -S --disassemble Test

***Read the ELF file***  
readelf -a Test
  
**Debug Console Commands**  
Use -exec to execute $(gdb) commands. Eg:  
-exec x/64b 0x615c20  
  
  
**Add shortcut to launch tasks**  
Configure Ctrl + Space for **Run Task...**  

**Recommended Extensions:**  
  C/C++  
