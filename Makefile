#Specify compiler
CC=g++
CFLAGS=-g

#Specify linker
LINK=g++

.PHONY : all
all : Test

Test : Test.o
	$(LINK) -o Test Test.o

Test.o : Test.cpp
	$(CC) $(CFLAGS) -c Test.cpp -o Test.o -std=c++11

#Clean target
.PHONY : clean
clean :
	rm Test.o Test

